const { timeStamp } = require("console");

// Handle function of class is used in js-logger library for the display and storage of logs
class LogHandler {
    constructor(logStore) {
        this.logStore = logStore;
    }

    // Stores message into logStore and displays log
    handle(messages, context) {
        let timeStamp = new Date().getTime();
        this.logStore.store(messages[0], context.level, timeStamp);

        console.log('%s [%s] - %s', timeStamp, messages[0], context.level);
    }
}

module.exports = LogHandler;
