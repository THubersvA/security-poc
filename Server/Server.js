import DigiSigner from './Lib/DigiSigner';

const logger = require('js-Logger');
const express = require('express');
const fs = require('fs');
const path = require('path');
const signer = new DigiSigner();
const cert = fs.readFileSync(path.join(__dirname,
    'certificate.der'), 'utf8');
const app = express();

logger.debug("OOF");

app.use(
    express.urlencoded({
        extended: true
    })
);

let certStore = new Map([["My Name", cert]]);

app.use(express.json())

app.listen(3000, () => {
    console.log("Server started!");
});

console.log(certStore);

app.post('/', (req, res) => {
    if (certStore.has(req.body.data.name)) {
        let certificate = certStore.get(req.body.data.name);
        let publicKey = crypto.createPublicKey(certificate).export({type:'spki', format:'pem'});

        console.log(signer.signData());

        res.writeHead(200);
    }
    else {
        res.writeHead(400);
    }
    res.end();
})
